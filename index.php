<?php
class DataDB extends SQLite3
{
    function __construct()
    {
        $this->open('data.db');
    }

    function get_config()
    {
        return($this->querySingle('SELECT * FROM arpi_config', true));
    }
}

/*
* INDEX ARCHI
*
* @ Arnaud Ritti
*
*/
session_start();

global $data;
global $db;
$db = new DataDB();

$config = $db->get_config();

define("_APP_FOLDER_", basename(dirname(__FILE__)), true);
define("_ROOT_", "http://".$_SERVER['SERVER_NAME']."/"._APP_FOLDER_."/", true);
define("_PATH_", $_SERVER['DOCUMENT_ROOT']."/"._APP_FOLDER_."/", true);
define("_ADMIN_", _ROOT_."admin/", true);
define("_TPL_", _ROOT_."theme/", true);
define("_CUR_TPL_", _ROOT_."theme/".$config["current_theme"], true);

define("_F_ADMIN_", _PATH_."admin/", true);
define("_F_TPL_", _PATH_."theme/", true);
define("_F_CUR_TPL_", _PATH_."theme/".$config["current_theme"], true);
include(_PATH_."core/front_end.php");
include(_PATH_."core/back_end.php");


$C = "index";
if(isset($_GET['c'])){
    $C = $_GET['c'];
}

if($C == "index"){
    $front = new \ARPI_CMS\front_end();
}elseif($C == "arpin"){
    $back = new \ARPI_CMS\back_end();
}else{
    header('Location: '._ROOT_);
}



?>